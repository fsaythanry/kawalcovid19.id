import * as React from 'react';
import { NextPage } from 'next';

import { wp } from 'utils/api';
import { WordPressPostIndex, WordPressUser } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { Heading, Stack, Text, Box, themeProps } from 'components/design-system';
import { PostIndexCard } from 'modules/posts-index';
import {
  Hero,
  ArticlesListGrid,
  ImportantLinksSection,
  SocialMediaSection,
  CasesSection,
} from 'modules/home';
import getAuthorsDetail from 'utils/wp/getAuthorsDetail';
import styled from '@emotion/styled';
import Link from 'next/link';
import { logEventClick } from 'utils/analytics';

interface IndexPageProps {
  information?: WordPressPostIndex[];
  infographic?: WordPressPostIndex[];
  verification?: WordPressPostIndex[];
  sticky?: WordPressPostIndex[];
  authors?: Record<number, WordPressUser>;
  errors?: string;
}

const Section = Content.withComponent('section');

const TextLink = Text.withComponent('a');

export interface CategorySectionProps {
  posts?: WordPressPostIndex[];
  authors?: Record<number, WordPressUser>;
  slug?: string;
  title: string;
  learnMore?: string;
  hasExcerpt?: boolean;
}

const CategorySection: React.FC<CategorySectionProps> = ({
  posts,
  authors,
  slug,
  title,
  learnMore,
  hasExcerpt,
}) => {
  const BaseBox = styled(Box)`
    ${themeProps.mediaQueries.md} {
      display: grid;
      grid-template-areas:
        'secthead readmore'
        'articles articles';
      align-items: center;
    }
  `;

  const SectionHeading = styled(Heading)`
    margin-bottom: ${themeProps.space.md}px;

    ${themeProps.mediaQueries.md} {
      grid-area: secthead;
      margin-bottom: ${themeProps.space.xl}px;
    }
  `;

  const ReadmoreBox = styled(Box)`
    margin-top: ${themeProps.space.md}px;

    ${themeProps.mediaQueries.md} {
      grid-area: readmore;
      margin-top: 0px;
      margin-bottom: ${themeProps.space.xl}px;
      justify-self: end;
    }
  `;

  const ReadmoreLink = styled(TextLink)`
    text-decoration: none;

    &:hover,
    &:focus {
      text-decoration: underline;
    }
  `;

  return posts?.length ? (
    <BaseBox mb="xxl">
      <SectionHeading variant={800} as="h2">
        {title}
      </SectionHeading>
      <ArticlesListGrid>
        {posts.map(post => (
          <PostIndexCard
            key={post.slug}
            post={post}
            author={authors?.[post.author]}
            hasExcerpt={hasExcerpt}
          />
        ))}
      </ArticlesListGrid>
      {slug && learnMore && (
        <ReadmoreBox>
          <Link href="/category/[slug]" as={`/category/${slug}`} passHref>
            <ReadmoreLink
              variant={500}
              fontWeight={600}
              color="primary02"
              onClick={() => logEventClick(learnMore)}
            >
              {learnMore} &rarr;
            </ReadmoreLink>
          </Link>
        </ReadmoreBox>
      )}
    </BaseBox>
  ) : null;
};

const IndexPage: NextPage<IndexPageProps> = ({
  information,
  infographic,
  verification,
  sticky,
  authors,
}) => (
  <PageWrapper pageTitle="Beranda">
    <Hero />
    <Section>
      <Column>
        <Stack spacing="xxl">
          <CasesSection />
          <CategorySection
            posts={sticky}
            authors={authors}
            slug="bacaan"
            title="Bacaan Pilihan"
            learnMore="Lihat Semua Bacaan"
            hasExcerpt
          />
          <CategorySection
            posts={information}
            authors={authors}
            slug="artikel"
            title="Informasi Terkini"
            learnMore="Lihat Semua Berita"
            hasExcerpt
          />
          <CategorySection
            posts={infographic}
            authors={authors}
            slug="infografik"
            title="Infografik Terbaru"
            learnMore="Lihat Semua Infografik"
          />
          <CategorySection
            posts={verification}
            authors={authors}
            slug="verifikasi"
            title="Periksa Fakta"
            learnMore="Lihat Semua Fakta"
          />
          <ImportantLinksSection />
          <SocialMediaSection />
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
);

export async function getStaticProps() {
  try {
    const [informationCategory, infographicCategory, verificationCategory] = await Promise.all([
      wp('wp/v2/categories', {
        slug: 'artikel',
        _fields: 'id,count,description,name,slug',
      }),
      wp('wp/v2/categories', {
        slug: 'infografik',
        _fields: 'id,count,description,name,slug',
      }),
      wp('wp/v2/categories', {
        slug: 'verifikasi',
        _fields: 'id,count,description,name,slug',
      }),
    ]);

    if (informationCategory && infographicCategory && verificationCategory) {
      let sticky: WordPressPostIndex[] | undefined;
      let information: WordPressPostIndex[] | undefined;
      let infographic: WordPressPostIndex[] | undefined;
      let verification: WordPressPostIndex[] | undefined;
      let authors: Record<string, WordPressUser> | undefined;

      const [
        unfilteredInformationPosts,
        unfilteredInfographicPosts,
        unfilteredVerificationPosts,
        unfilteredStickyPosts,
      ] = await Promise.all([
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: informationCategory[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: infographicCategory[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: verificationCategory[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
        wp<WordPressPostIndex[]>('wp/v2/posts', {
          sticky: true,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        }),
      ]);

      if (Array.isArray(unfilteredInformationPosts)) {
        const posts = unfilteredInformationPosts.filter(post => post.type === 'post');
        information = posts.slice(0, 4);
      }

      if (Array.isArray(unfilteredInfographicPosts)) {
        const posts = unfilteredInfographicPosts.filter(post => post.type === 'post');
        infographic = posts.slice(0, 4);
      }

      if (Array.isArray(unfilteredVerificationPosts)) {
        const posts = unfilteredVerificationPosts.filter(post => post.type === 'post');
        verification = posts.slice(0, 2);
      }

      if (Array.isArray(unfilteredStickyPosts)) {
        const posts = unfilteredStickyPosts.filter(post => post.type === 'post');
        sticky = posts.slice(0, 2);
      }

      if (information?.length || infographic?.length || verification?.length) {
        const authorsIdList = [
          ...(information || []),
          ...(infographic || []),
          ...(verification || []),
          ...(sticky || []),
        ].map(post => post.author);

        const uniqueAuthors = [...new Set(authorsIdList)];
        const authorsDetail = await getAuthorsDetail(uniqueAuthors);
        const map = authorsDetail.reduce<Record<string, WordPressUser>>((obj, item) => {
          // eslint-disable-next-line no-param-reassign
          obj[item.id] = item;
          return obj;
        }, {});

        authors = map;
      }

      return { props: { information, infographic, verification, sticky, authors } };
    }

    throw new Error('Failed to fetch posts');
  } catch (err) {
    return { props: { errors: err.message } };
  }
}

export default IndexPage;
