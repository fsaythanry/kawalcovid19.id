import * as React from 'react';
import usePrefetch from 'utils/usePrefetch';

const QiscusWidget: React.FC = () => {
  // Preload Qiscus assets
  // usePrefetch('preload', 'https://rsms.me/inter/inter.css', 'style');
  // usePrefetch('preload', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600', 'style');

  // Preconnect required resources
  usePrefetch('preconnect', 'https://qismo.qiscus.com');
  usePrefetch('preconnect', 'https://qiscus-sdk.s3-ap-southeast-1.amazonaws.com');
  usePrefetch('preconnect', 'https://s3-ap-southeast-1.amazonaws.com');

  return (
    <>
      {/* eslint-disable react/no-danger */}
      <script
        dangerouslySetInnerHTML={{
          __html: `document.addEventListener('DOMContentLoaded', function() {
          var s,t; s = document.createElement('script'); s.type = 'text/javascript';
          s.src = 'https://s3-ap-southeast-1.amazonaws.com/qiscus-sdk/public/qismo/qismo-v2-n.js'; s.async = true;
          if(s.onload != 'undefined') { s.onload = function() { new Qismo("fea-vcdx2gjkuddtpvxva"); } }
          else { s.onreadystatechange = function() { new Qismo("fea-vcdx2gjkuddtpvxva"); } }
          t = document.getElementsByTagName('script')[0]; t.parentNode.insertBefore(s, t);
      });`,
        }}
      />
      {/* eslint-enable react/no-danger */}
    </>
  );
};

export default QiscusWidget;
