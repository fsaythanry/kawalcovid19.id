export interface RenderedContent {
  rendered: string;
  protected?: boolean;
}

export interface WordPressCategory {
  id: number;
  count: string;
  description: string;
  name: string;
  slug: string;
}

export interface WordPressUser {
  id: number;
  name: string;
  description: string;
  slug: string;
  avatar_urls: Record<string, string>;
}

export interface WordPressPostIndex {
  id: number;
  date_gmt: string;
  modified_gmt: string;
  type: string;
  slug: string;
  title: RenderedContent;
  excerpt: RenderedContent;
  author: number;
}

export interface WordPressPost extends WordPressPostIndex {
  categories: number[];
  content: RenderedContent;
}
