import querystring from 'query-string';
import fetch from './fetch';

const API_BASE = 'http://18.141.204.243/?rest_route=/';

export default async function wp<TResponse = any>(
  input: string,
  query?: { [key: string]: any },
  init?: RequestInit
): Promise<TResponse> {
  const path = input.replace(/^\/+/g, '');
  const qs = query ? `&${querystring.stringify(query)}` : '';
  const data = await fetch<TResponse>(`${API_BASE}${path}${qs}`, init);
  return data;
}
